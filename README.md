# README #

This is a simple bible app to view the bible. It is currently a WIP and I plan to add many more features (once I get the time).

### How do I get set up? ###

* Clone the master branch to your local machine
* Go to [the Bible.org API website](http://bibles.org/pages/api) and follow the prompts to create your own api key
* Create a resource file under the standard 'values' folder (you can call it anything, but I have called mine api_key.xml)
* Create a string resource in there called 'api_key' and insert your api key 
```
#!xml

<string name="api_key">YOUR_API_KEY_HERE</string>
```
* Compile and run!

### Licence ###

The MIT License (MIT)
Copyright (c) 2016 Jordan Huizenga

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.