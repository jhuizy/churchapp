package frca.churchapp.model

import com.google.gson.annotations.SerializedName

data class VersionEnvelope(var response: ResponseEnvelope)

data class ResponseEnvelope(var versions: List<Version>)

data class Version(var id: String,
                   var name: String)

data class BooksResponseEnvelope(var response: BookResponse)

data class BookResponse(var books: List<Book>)

data class Book(var name: String,
                var parent: Book.BookParent,
                @SerializedName("abbr") var abbreviation: String,
                var id: String,
                var previous: BibleReference?,
                var next: BibleReference?) {

    data class BookParent(var version: BibleReference)
}

data class ChapterResponseEnvelope(var response: ChapterResponse)

data class ChapterResponse(var chapters: List<Chapter>)

data class Chapter(var chapter: String,
                   var label: String,
                   var next: BibleReference,
                   var previous: BibleReference)

data class VerseResponseEnvelope(var response: VerseResponse)

data class VerseResponse(var verses: List<Verse>)

data class Verse(var parent: BibleReference,
                 var reference: String,
                 var text: String,
                 var verse: String,
                 var id: String,
                 var next: BibleReference,
                 var previous: BibleReference)

data class BibleReference(var path: String, var name: String, var id: String)