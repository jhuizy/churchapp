package frca.churchapp.lib.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import frca.churchapp.R

interface AdapterDelegate<VH : RecyclerView.ViewHolder> {

    fun onBindViewHolder(holder: VH, position: Int)

    fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH

    fun getItemCount(): Int

}

class BaseRecyclerViewAdapter<VH : RecyclerView.ViewHolder>(val delegate: AdapterDelegate<VH>) : RecyclerView.Adapter<VH>(), AdapterDelegate<VH> by delegate

class SimpleListAdapter<T>(val items: List<T>, val toString: (T) -> String = { it.toString() }, val onClick: (T) -> Unit = {}) : AdapterDelegate<SimpleListAdapter.ViewHolder> {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title?.text = toString(items[position])
        holder.itemView?.setOnClickListener { onClick(items[position]) }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context)?.inflate(R.layout.simple_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(val view: View?) : RecyclerView.ViewHolder(view) {
        val title = view?.findViewById(R.id.textview_title) as? TextView
    }

}

fun <VH : RecyclerView.ViewHolder> RecyclerView.adapterFromDelegate(delegate: AdapterDelegate<VH>) {
    adapter = BaseRecyclerViewAdapter(delegate)
}