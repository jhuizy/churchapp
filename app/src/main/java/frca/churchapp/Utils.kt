package frca.churchapp

import android.app.Fragment
import android.content.DialogInterface
import android.support.v7.app.AlertDialog

/**
 * Created by Jordan on 10/21/2015.
 */

fun Fragment.showDialog(title: String, message: String = title,
                        onPositiveClick: (dialogInterface: DialogInterface, which: Int) -> Unit = { d, w -> d.dismiss() }) {
    AlertDialog.Builder(this.context)
            .setMessage(message)
            .setTitle(title)
            .setPositiveButton("OK", onPositiveClick)
            .setNegativeButton("Cancel") { dialogInterface, l -> dialogInterface.dismiss() }
            .show()
}

fun Fragment.showError(error: Throwable) {
    this.showDialog("Error", error.message ?: "Unknown")
}