package frca.churchapp.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Jordan on 10/20/2015.
 */
public abstract class BibleFragment(var layoutResID: Int, var title: String, var menuTitle: String = title) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(layoutResID, container, false)
    }
}
