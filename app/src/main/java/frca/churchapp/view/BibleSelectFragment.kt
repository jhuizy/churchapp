package frca.churchapp.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import frca.churchapp.Defaults
import frca.churchapp.R
import frca.churchapp.lib.view.SimpleListAdapter
import frca.churchapp.lib.view.adapterFromDelegate
import frca.churchapp.service.createBibleService
import kotlinx.android.synthetic.main.fragment_bible_select.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class BibleSelectFragment : BibleFragment(R.layout.fragment_bible_select, "Search") {

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val service = createBibleService()
        recyclerview.layoutManager = LinearLayoutManager(context)

        service.getBooks(Defaults.VERSION)
                .flatMap { Observable.from(it.response.books) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { recyclerview.adapterFromDelegate(SimpleListAdapter(it, { it.name })) }
    }
}