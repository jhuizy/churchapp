package frca.churchapp.service

import android.util.Base64
import frca.churchapp.Constants
import frca.churchapp.model.BooksResponseEnvelope
import frca.churchapp.model.ChapterResponseEnvelope
import frca.churchapp.model.VerseResponseEnvelope
import frca.churchapp.model.VersionEnvelope
import retrofit.RestAdapter
import retrofit.http.GET
import retrofit.http.Path
import rx.Observable

interface BibleService {

    @[GET("/versions.js")]
    fun getVersions(): Observable<VersionEnvelope>

    @[GET("/versions/{id}/books.js")]
    fun getBooks(@[Path("id")] id: String): Observable<BooksResponseEnvelope>

    @[GET("/books/{versionid}:{bookid}/chapters.js")]
    fun getChapters(@[Path("versionid")] versionid: String,
                    @[Path("bookid")] bookid: String): Observable<ChapterResponseEnvelope>

    @[GET("/chapters/{versionid}:{bookid}.{chapternumber}/verses.js")]
    fun getVerses(@[Path("versionid")] versionid: String,
                  @[Path("bookid")] bookid: String,
                  @[Path("chapternumber")] chapterNumber: Int): Observable<VerseResponseEnvelope>
}

fun createBibleService() = RestAdapter.Builder()
        .setEndpoint(Constants.BIBLE_API_BASE_URL)
        .setLogLevel(RestAdapter.LogLevel.BASIC)
        .setRequestInterceptor { req ->
            var creds = "${Constants.BIBLE_API_KEY}:X"
            var auth = "Basic ${Base64.encodeToString(creds.toByteArray(), Base64.NO_WRAP)}"
            req.addHeader("Authorization", auth)
        }
        .build()
        .create(BibleService::class.java)
